<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 04-30-2024 and will be end of life on 04-30-2025. The capabilities of this Pre-Built have been replaced by the [IAP - Workflow Utilities](https://gitlab.com/itentialopensource/pre-built-automations/iap-workflow-utilities)

<!-- Update the below line with your Pre-Built name -->
# Chunk Large Array For Child Job 

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Additional Information](#additional-information)

## Overview

<!-- Write a few sentences about the Pre-Built and explain the use case(s) -->
<!-- Avoid using the word Artifact. Please use Pre-Built, Pre-Built Transformation or Pre-Built Automation -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->
This Pre-Built allows child jobs to iterate over an array with size larger than 10,000. Users may input a large array and a desired chunk size (<=10,000) and the pre-built will separate the data into chunks that can then be passed to a child job in groups. This Pre-Built overcomes the size limitation of 10,000 elements for child job loop data. 

<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->

## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2022.1`


## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button.

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this Pre-Built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the Pre-Built:

<!-- Explain the main entrypoint(s) for this Pre-Built: Automation Catalog item, Workflow, Postman, etc. -->
The chunkArrayWrapper can be run as a child job with the user input data in array format and a specified chunk size. The user must manually specify the child job they want to be run on their data in the chunkArrayWrapperChild workflow.

1. Add chunkArrayWrapper as a child workflow in any parent workflow
2. Specify data for the two job variables data_array (the data the child flow run with) and chunkSize (10,000 or less). 
3. Open chunkArrayWrapperChild and specify the child flow to be run on the data. Loop type can also be changed to be either parallel or sequential.
4. Run parent workflow 

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
